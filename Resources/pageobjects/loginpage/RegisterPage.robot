*** Settings ***
Library  Selenium2Library

*** Variables ***

${FIRSTNAME_INPUT}  name=customer_firstname
${LASTNAME_INPUT}  id=lastNameCustomerInput
${PASSWORD_INPUT}  id=passwordCustomerInput
${COMPANY_INPUT}  id=companyAddressInput
${ADDRESS1_INPUT}  id=address1CustomerInput
${ADDRESS2_INPUT}  id=address2CustomerInput
${CITY_INPUT}  id=cityCustomerInput
${STATE_INPUT}  id=stateCustomerInput
${POSTAL_INPUT}  id=postcodeInput
${MOBILE_PHONE_INPUT}  id=MobilePhoneCustomerInput
${HOME_PHONE_INPUT}  id=HomePhoneCustomerInput
${OTHER_INPUT}  id=otherCustomerInput
${SUBMIT_BUTTON}    id=submitAccount
${ERROR_CONTAINER}  css=div.alert.alert-danger

*** Keywords ***
Set First Name
    [Arguments]  ${first_name}
    input text  ${FIRSTNAME_INPUT}  ${first_name}
Set Last Name
    [Arguments]  ${last_name}
    input text  ${LASTNAME_INPUT}  ${last_name}
Set Password
    [Arguments]  ${password}
    input text  ${PASSWORD_INPUT}  ${password}
Set Company
    [Arguments]  ${company}
    input text  ${COMPANY_INPUT}  ${company}
Set Address1
    [Arguments]  ${address1}
    input text  ${ADDRESS1_INPUT}  ${address1}
Set Address2
    [Arguments]  ${address2}
    input text  ${ADDRESS2_INPUT}  ${address2}
Set City
    [Arguments]  ${city}
    input text  ${CITY_INPUT}  ${city}l
Set State
    [Arguments]  ${state}
    select from list by label  ${state_input}   ${state}
Set Postal Code
    [Arguments]  ${postal_code}
    input text  ${POSTAL_INPUT}  ${postal_code}
Set Mobile Phone
    [Arguments]  ${mobile_phone}
    input text  ${MOBILE_PHONE_INPUT}  ${mobile_phone}
Set Home Phone
    [Arguments]  ${home_phone}
    input text  ${HOME_PHONE_INPUT}  ${home_phone}
Set Other
    [Arguments]  ${other}
    input text  ${OTHER_INPUT}  ${other}
Submit Register
    click button  ${SUBMIT_BUTTON}
Verify that registration screen is not displayed
    page should not contain element   ${FIRSTNAME_INPUT}
Verify that registration screen is displayed
    wait until page contains element    ${FIRSTNAME_INPUT}
Verify That Validation Error Occurs
    page should contain element  ${ERROR_CONTAINER}
Verify That Validation Error Does Not Occurs
    page should not contain element  ${ERROR_CONTAINER}




