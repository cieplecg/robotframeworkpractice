*** Settings ***
Documentation  This is suite for verification of login screen

Resource  ../Resources/Common.robot
Resource  ../Resources/pageobjects/homepage/HomePage.robot
Resource  ../Resources/pageobjects/loginpage/LoginPage.robot
Resource  ../Resources/pageobjects/loginpage/RegisterPage.robot

Suite Setup  Common.Begin Web Test
Suite Teardown  Common.End Web Test

*** Variables ***
${random_string}    Common.Generate Random Variable

*** Test Cases ***
Register with valid input data
    [Tags]  Register
    HomePage.Verify if logged out
    HomePage.Go To Login Page
    LoginPage.Register As   ${random_string}@yahoo.com
    RegisterPage.Verify that registration screen is displayed
