*** Settings ***
Library  Selenium2Library

*** Variables ***
${LOGIN_INPUT}  id=email
${PASSWORD_INPUT}  id=passwd
${LOGIN_SUBMIT}  id=SubmitLogin
${REGISTER_INPUT}  id=email_create
${REGISTER_SUBMIT}  id=SubmitCreate

*** Keywords ***
Set Login
    [Arguments]  ${login}
    input text  ${LOGIN_INPUT}  ${login}

Set Password
    [Arguments]  ${password}
    input text  ${PASSWORD_INPUT}   ${password}

Set Register Email
    [Arguments]  ${register_email}
    input text  ${REGISTER_INPUT}   ${register_email}

Login As
    [Arguments]  ${login}   ${password}
    Set Login   ${login}
    Set Password    ${password}
    click button  ${LOGIN_SUBMIT}

Register As
    [Arguments]  ${email}
    Set Register Email  ${email}
    click button  ${REGISTER_SUBMIT}