*** Settings ***
Library  Selenium2Library

*** Variables ***
${NAVIGATION_BAR}  css=div.header_user_info.sf-menu
${SIGN_IN_BUTTON}  link=Sign in
${LOGOUT_BUTTON}  link=Sign out

*** Keywords ***
Verify if logged in
    Page Should Contain Link  ${LOGOUT_BUTTON}
#    wait until page contains element  ${LOGOUT_BUTTON}

Verify if logged out
    Page Should Contain Link  ${SIGN_IN_BUTTON}
#    wait until page contains element  ${SIGN_IN_BUTTON}

Go to login page
    click link  ${SIGN_IN_BUTTON}
