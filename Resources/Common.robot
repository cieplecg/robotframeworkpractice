*** Settings ***
Library  Selenium2Library
Library  String

*** Variables ***
${BROWSER} =  firefox
${ENVIRONMENT} =  dev
&{URL}  dev=http://www.automationpractice.com  prod=http://www.automationpractice.com

*** Keywords ***
Begin Web Test
    Open Browser    ${URL.${ENVIRONMENT}}  ${BROWSER}
    maximize browser window

End Web Test
    Close Browser

Generate Random Variable
    ${random_string} =  Generate Random String  15  [LETTERS]
    [Return]    ${random_string}